/**
 * Created by Tanaji on 1/23/2017.
 */

var empApp = angular.module('empApp', ['ngRoute']);

// configure our routes
empApp.config(function ($routeProvider) {
    $routeProvider

    // route for the home page
        .when('/home', {
            templateUrl: 'pages/home.html'
        })

        // route for the add page
        .when("/add", {
            templateUrl: "pages/add.html"
        })

        // route for the contact page
        .when('/contact', {
            templateUrl: 'pages/contact.html'
        });
});

