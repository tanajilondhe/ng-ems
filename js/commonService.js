/**
 * Created by Tanaji on 1/26/2017.
 */

empApp.service('commonService', function () {

    this.getUid = function () {
        return randomGenerator() + '-' + randomGenerator() + '-' + randomGenerator() + '-' +
            randomGenerator() + '-' + randomGenerator();
    }

    function randomGenerator() {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }

});
