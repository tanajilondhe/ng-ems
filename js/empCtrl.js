/**
 * Created by Tanaji on 1/23/2017.
 */

empApp.controller('empCtrl', function ($rootScope, $scope, $location, commonService, empService) {

    $scope.save = function (emp) {

        empService.save(emp);

        $scope.employeeList = empService.getEmployees();

        $scope.employee = {}

        $location.path("/home");
    }

    $scope.view = function (emp) {

        $scope.isEditable = true;

        $scope.employee = emp;

        $location.path("/add");
    }

    $scope.edit = function (emp) {

        $scope.isEditable = false;

        //this angular.copy() avoids two binding
        // if user click on cancel button it doesn't update list
        $scope.employee = angular.copy(emp);

        $location.path("/add");
    }

    $scope.delete = function (employee) {

        empService.delete(employee);

        $scope.employeeList = empService.getEmployees();
    }

    $scope.cancel = function () {

        $scope.isEditable = false;

        $scope.employee = {};

        $location.path("/home");
    }

});