/**
 * Created by Tanaji on 1/26/2017.
 */

empApp.service('empService', function ($rootScope, commonService) {

    var employeeList = [];

    this.getEmployees = function () {
        return employeeList;
    }

    this.indexOf = function (array, employee) {
        var arrayIndex = -1;
        angular.forEach(array, function (element, index) {
            if (element.id === employee.id) {
                arrayIndex = index;
            }
        });
        return arrayIndex;
    }

    this.save = function (employee) {

        var index = this.indexOf(employeeList, employee);

        if (index === -1) {
            employee.id = commonService.getUid();
            employeeList.push(employee);
        } else {
            employeeList[index] = employee;
        }
    }

    this.delete = function (employee) {
        var index = this.indexOf(employeeList, employee);
        employeeList.splice(index, 1);
    }
});